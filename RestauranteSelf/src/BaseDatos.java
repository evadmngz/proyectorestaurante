
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DAM122
 */
public class BaseDatos {
    
     private Connection conn;
    private static BaseDatos INSTANCE;

    /**
     * Patrón de diseño singleton
     */
    private BaseDatos() 
    {
       String connectionURL = "jdbc:mysql://localhost:3306/ProyectoRestaurante";

        try {
            Class.forName("com.mysql.jdbc.Driver");

            Properties properties = new Properties();
            properties.setProperty("user", "root");
            properties.setProperty("password", "");
            properties.setProperty("useSSL", "false");
            properties.setProperty("autoReconnect", "true");
            conn = (Connection) DriverManager.getConnection(connectionURL, properties);

            if (conn == null) {
                System.out.println("error en conexion");
            } else {
                System.out.println("Conexion correcta");
            }
            
            
           
        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (ClassNotFoundException ex) {
            System.out.println("ClassNotFoundException: " + ex.getMessage());
        }
    }
    
   
    
    public static BaseDatos getInstance()
    {
        if(INSTANCE == null)
            INSTANCE = new BaseDatos();
        return INSTANCE;
    }
    
    
    public Connection getConnection()
    {
        return conn;
    }
}
