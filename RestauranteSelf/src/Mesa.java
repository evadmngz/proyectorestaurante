
import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DAM116
 */
public class Mesa implements Serializable{

    private String nombre;
    private int numAsientos;
    private boolean estado;

    public Mesa(String nombre, int numAsientos, boolean estado) {
        this.nombre = nombre;
        this.numAsientos = numAsientos;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNumAsientos() {
        return numAsientos;
    }

    public void setNumAsientos(int numAsientos) {
        this.numAsientos = numAsientos;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Mesa{" + "nombre=" + nombre + ", numAsientos=" + numAsientos + ", estado=" + estado + '}';
    }

}
