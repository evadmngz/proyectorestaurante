/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author DAM116
 */
public class Segundo {
    
    private String nombreSegundo;
    private float precioSegundo;

    public Segundo(String nombreSegundo, float precioSegundo) {
        this.nombreSegundo = nombreSegundo;
        this.precioSegundo = precioSegundo;
    }

    public String getNombreSegundo() {
        return nombreSegundo;
    }

    public void setNombreSegundo(String nombreSegundo) {
        this.nombreSegundo = nombreSegundo;
    }

    public float getPrecioSegundo() {
        return precioSegundo;
    }

    public void setPrecioSegundo(float precioSegundo) {
        this.precioSegundo = precioSegundo;
    }

    @Override
    public String toString() {
       return nombreSegundo+" ("+precioSegundo+"€)";
    }
    
    
    
}
