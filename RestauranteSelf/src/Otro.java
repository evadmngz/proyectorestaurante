/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author DAM116
 */
public class Otro {
    
    private String nombreOtro;
    private float precioOtro;

    public Otro(String nombreOtro, float precioOtro) {
        this.nombreOtro = nombreOtro;
        this.precioOtro = precioOtro;
    }

    public String getNombreOtro() {
        return nombreOtro;
    }

    public void setNombreOtro(String nombreOtro) {
        this.nombreOtro = nombreOtro;
    }

    public float getPrecioOtro() {
        return precioOtro;
    }

    public void setPrecioOtro(float precioOtro) {
        this.precioOtro = precioOtro;
    }

    @Override
    public String toString() {
       return ""+nombreOtro+" ("+precioOtro+"€)";
    }
    
    
    
}
