/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author DAM116
 */
public class Primero {
    
    private String nombrePrimero;
    private float precioPrimero;

    public Primero(String nonmbre, float precio) {
        this.nombrePrimero = nonmbre;
        this.precioPrimero = precio;
    }

    public String getNonmbre() {
        return nombrePrimero;
    }

    public void setNonmbre(String nonmbre) {
        this.nombrePrimero = nonmbre;
    }

    public float getPrecio() {
        return precioPrimero;
    }

    public void setPrecio(float precio) {
        this.precioPrimero = precio;
    }

    @Override
    public String toString() {
        return ""+nombrePrimero+" ("+precioPrimero+"€)";
    }
    
    
    
}
