
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DAM116
 */
public class GestionFicheros {
private File f;

    public GestionFicheros(String nombre) {
        f=new File(nombre);
    }
    
public  ArrayList<Mesa> leerFichero() {
        ObjectInputStream ois = null;
        Mesa auxMesa;
        ArrayList<Mesa> auxMesas = new ArrayList();
        try {

            ois = new ObjectInputStream(new FileInputStream(f));

            while (true) {

                    
                    auxMesa = (Mesa) ois.readObject();
                     auxMesas.add(auxMesa);
                     System.out.println(auxMesa.toString());
                } 
               
        }catch (ClassNotFoundException ex){
            Logger.getLogger(GestionFicheros.class.getName()).log(Level.SEVERE, null, ex);
        } catch (EOFException ex) {
            System.out.println("Fin fichero");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GestionFicheros.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(GestionFicheros.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException ex) {
                    Logger.getLogger(GestionFicheros.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return auxMesas;
    }

    public  void guardarFichero( ArrayList<Mesa> auxMesas) {
        ObjectOutputStream obs = null;
        try {
          
                obs = new ObjectOutputStream(new FileOutputStream(f));
            
            for (Mesa auxM : auxMesas) {
                System.out.println(auxM.toString());
                obs.writeObject(auxM);
            }

        } catch (IOException ex) {
            System.err.println(ex.toString());
        } finally {
            try {
                if (obs != null) {
                    obs.close();
                }
            } catch (IOException ex) {
                System.out.println("No se pudo cerrar");
            }
        }
    }

    public File getF() {
        return f;
    }

    public void setF(File f) {
        this.f = f;
    }
   

}
