
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DAM116
 */
public class Comedor {

    ArrayList<Mesa> comedor;
    private boolean estadoComedor = false;

    public Comedor() {
        comedor = new ArrayList<Mesa>();
        estadoComedor = estadoComedor;
    }

    public boolean isEstadoComedor() {
        return estadoComedor;
    }

    public void setEstadoComedor(boolean estadoComedor) {
        this.estadoComedor = estadoComedor;
    }

    @Override
    public String toString() {
        return "Comedor{" + "estadoComedor=" + estadoComedor + '}';
    }

    public void estadoComedor() {
        int contador = 0;
        for (Mesa m : comedor) {
            if (m.isEstado() == true) {
                //Entiendo que true es que la mesa este ocupada              
                contador++;
            }
            if(contador ==4){
                estadoComedor = true;
            }
        }
    }

}
