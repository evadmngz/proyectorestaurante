
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author DAM122
 */
public class Metodos_BDD {
    
    public static ArrayList<Primero> cargarPrimero() {
        Connection con = BaseDatos.getInstance().getConnection();
       
        Statement sentencia;
       ArrayList<Primero> primeros = null;
        try{
        sentencia = con.createStatement();
        primeros = new ArrayList<Primero>();
            
           ResultSet rs = sentencia.executeQuery("select nombrePrimeros, precioPrimeros from primeros");

                while (rs.next()) {
                    
                    
                    String nombre = rs.getString("nombrePrimeros");
                    float precio = rs.getFloat("precioPrimeros");
                    
                     Primero primer = new Primero(nombre, precio);
                     primeros.add(primer);
                   
                }
        
        }catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
       return primeros;
    }
    
    public static ArrayList<Segundo> cargarSegundo() {
        Connection con = BaseDatos.getInstance().getConnection();
       
        Statement sentencia;
       ArrayList<Segundo> segundos = null;
        try{
        sentencia = con.createStatement();
        segundos = new ArrayList<Segundo>();
            
           ResultSet rs = sentencia.executeQuery("select nombreSegundos, precioSegundos from segundos");

                while (rs.next()) {

                   
                    String nombre = rs.getString("nombreSegundos");
                    float precio = rs.getFloat("precioSegundos");
                    
                     Segundo segun = new Segundo(nombre, precio);
                     segundos.add(segun);
                   
                }
        
        }catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
       return segundos;
    }
    
    public static ArrayList<Otro> cargarOtros() {
        Connection con = BaseDatos.getInstance().getConnection();
       
        Statement sentencia;
       ArrayList<Otro> otros = null;
        try{
        sentencia = con.createStatement();
        otros = new ArrayList<Otro>();
            
           ResultSet rs = sentencia.executeQuery("select nombreOtros, precioOtros from otros");

                while (rs.next()) {
                    
                   
                    String nombre = rs.getString("nombreOtros");
                    float precio = rs.getFloat("precioOtros");
                    
                     Otro primer = new Otro(nombre, precio);
                     otros.add(primer);
                   
                }
        
        }catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
       return otros;
    }
    
}
